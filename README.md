# SSH

## Javier Moyano - isx21762202

ASIX M06-ASO 2020-2021

### Imágenes

* **isx21762202/ssh20:base** Host base amb el *servei SSH* engegat. El host està configurat 
  amb PAM per permetre usuaris locals (unix01, etc) i usuaris de LDAP (pere,...). Cal
  engegar el container conjuntament amb el servei LDAP.


* **isx21762202/ssh20:sshfs** Host *client* que accedeix al servidor SSH. Aquest host client 
  està configurat per muntar els homes dels usuaris via *sshfs* del servidor SSH. S'ha 
  configurat *system-auth* per uar *pam_mount* i configurat *pam_mount.conf.xml* per muntar
  un recurs de xarxa al home dels usuaris via *SSHFS*. 


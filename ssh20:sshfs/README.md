# SSH

## Javier Moyano - isx21762202

ASIX M06-ASO 2020-2021

### Imágenes

* **isx21762202/ssh20:sshfs**  Host *client* que accedeix al servidor SSH. Aquest host client
  està configurat per muntar els homes dels usuaris via *sshfs* del servidor SSH. S'ha
  configurat *syste-auth* per usar *pam_mount* i configurat *pam_mount.conf.xml* per muntar
  un recurs de xarxa al home dels usuaris via *SSHFS*.
 
  Atenció, cal usar en el container --privileged per poder fer els muntatges nfs.

```
docker run --rm --name sshfs.edt.org --hostname sshfs.edt.org --net 2hisix --privileged --cap-add SYS_ADMIN --device /dev/fuse  --security-opt apparmor:unconfined -it isx21762202/ssh20:sshfs
```

